//
//  AppDelegate.swift
//  Battery Killer
//
//  Created by Tristan Reed on 1/09/2015.
//  Copyright (c) 2015 Trisreed Development. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!
    let statusItem = NSStatusBar.systemStatusBar().statusItemWithLength(-2)

    func applicationDidFinishLaunching(aNotification: NSNotification) {
        
        if let button = statusItem.button {
            button.image = NSImage(named: "BatteryKillerLogo")
            button.action = Selector("configurateBattery:")
        }
        
        let menu = NSMenu()
        menu.addItem(NSMenuItem(title: "Quit Battery Killer", action: Selector("terminate:"), keyEquivalent: "q"))
        statusItem.menu = menu
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
            self.startKillingBattery(self)
        })

    }
    
    func startKillingBattery(sender: AnyObject) {
        let effectiveConstant = UInt32.max
        while(effectiveConstant == effectiveConstant) {
            var uselessVariable = arc4random_uniform(effectiveConstant)
        }
    }

}

